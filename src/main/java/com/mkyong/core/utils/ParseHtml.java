package com.mkyong.core.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParseHtml {

    public ArrayList<String> parse(File file) throws IOException{
        ArrayList<String> listNamePng = new ArrayList<String>();
        Document doc = Jsoup.parse(file, "utf-8");
        Element divTag = doc.getElementById("tablecontents");

        try {
            Elements imgs = divTag.select("img");


            for (Element img : imgs){
                if (img.attr("class").equals("screenshot")){
                    String name = img.attr("src");
                    listNamePng.add(name);
                    //System.out.println(name);
                }
            }

        }catch (NullPointerException e){


        } finally {
            return listNamePng;
        }

    }


    public ArrayList<String> parseAllFile(List<File> files) throws IOException{
        ArrayList<String> listNamePng = new ArrayList<String>();

        for (File file : files) {
            listNamePng.addAll(parse(file));
        }
        return listNamePng;
    }
}
