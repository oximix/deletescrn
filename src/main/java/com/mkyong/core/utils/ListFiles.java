package com.mkyong.core.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListFiles {

    public ListFiles(String path) throws IOException {
        setAllFiles(search(path));
    }

    public List<File> getAll_NONEpng() {
        return all_NONEpng;
    }

    public void setAll_NONEpng() {
        ArrayList<File> lst_NONEpng = new ArrayList<File>();

        for(File lst : this.allFiles){
            if (lst.getName().contains("_NONE")){
                lst_NONEpng.add(lst);
            }
        }
        this.all_NONEpng = lst_NONEpng;
    }

    public List<File> getAll_html() {
        return all_html;
    }

    public void setAll_html() {
        List<File> all_html = new ArrayList<File>();

        for(File lst : this.allFiles){
            if (lst.getName().contains(".html") && !lst.getName().contains("screenshots.html")){
                all_html.add(lst);
            }
        }
        this.all_html = all_html;
    }

    public List<File> getUnused_NONEpng() {
        return unused_NONEpng;
    }

    public void setUnused_NONEpng() throws IOException {
        ParseHtml r = new ParseHtml();
        ArrayList<String> listNamePngUsedInHtml = r.parseAllFile(getAll_html());
        List<File> files = getAll_NONEpng();

        //из всего листа с png файлами удалить те, которые используются
        for (String namePng : listNamePngUsedInHtml) {
            for(File file : files) {
                if(file.getName().equals(namePng)) {
                    files.remove(file);
                    break;
                }
            }
        }
        this.unused_NONEpng = files;
    }

    public List<File> getAllFiles() {
        return allFiles;
    }

    public void setAllFiles(List<File> allFiles) {
        this.allFiles = allFiles;
    }

    private List<File> all_NONEpng;
    private List<File> all_html;
    private List<File> unused_NONEpng;
    private List<File> allFiles;

    public List<File> search(String path){
        File dir = new File(path); //path указывает на директорию
        File[] arrFiles = dir.listFiles();
        return  Arrays.asList(arrFiles);
    }

    public void deleteFiles(List<File> files) {
        for (File file : files)
            file.delete();
        System.out.println("Count of deleted files: " + files.size());
    }
}
